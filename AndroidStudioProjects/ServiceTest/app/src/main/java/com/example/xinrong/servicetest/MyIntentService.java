package com.example.xinrong.servicetest;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by xinrong on 2017/10/12.
 */

public class MyIntentService extends IntentService {

     MyIntentService(){
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d("MyIntentService", "Thread id is"+ Thread.currentThread().getId());
    }

    public void onDestroy(){
        super.onDestroy();
        Log.d("MyIntentService", "onDestroy executed");
    }
}
