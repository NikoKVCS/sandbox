﻿## 试验田项目

  你们当前权限为developer. 权限信息请参考 https://docs.gitlab.com/ee/user/permissions.html

# 步骤一：

- 每个人创建自己的分支 branch
- 每个人创建自己的文件[任何文件都可以]，并push到自己的远程分支上
- 将自己的分支通过 create merge request ，提交合并到主分支的请求
- 若master分支发生了更改[如 有新的commit，或者有人合并了分支到master分支上]则开始进行步骤二

# 步骤二：
- 将自己的分支与master分支保持同步
- 并将下面的名字，改成自己的名字。push到自己的远程分支
- 将自己的分支通过 create merge request ， 提交合并到主分支的请求

-张馨蓉
